import "./preloader.scss";
import React from "react";

function Preloader() {
  return (
    <div className="preloader loaded">
      <div className="preloader__row">
        <div className="preloader__item"></div>
        <div className="preloader__item"></div>
      </div>
    </div>
  );
}

export default Preloader;
