import "./MessageInput.scss";
import { v4 as uuidv4 } from "uuid";

function createOwnMessage(text) {
  if (text.value.trim().length < 1) return;
  let currentTime = new Date();

  if (String(currentTime.getMinutes()).length === 1) {
    currentTime = `${currentTime.getHours()}:0${currentTime.getMinutes()}`;
  } else {
    currentTime = `${currentTime.getHours()}:${currentTime.getMinutes()}`;
  }

  return {
    text: text.value,
    createdAt: currentTime,
    isOwnMessage: true,
    id: uuidv4(),
    user: "me",
  };
}

function MessageInput(props) {
  return (
    <div className="message-input">
      <input
        type="text"
        className="message-input-text"
        placeholder="Put your message..."
      />
      <button
        className="message-input-button"
        onClick={() => {
          const input = document.querySelector(".message-input-text");
          const messageObj = createOwnMessage(input);
          if(messageObj){
            props.addMessage(messageObj);
          }
          input.value = "";
        }}
      >
        Send Message
      </button>
    </div>
  );
}

export default MessageInput;
