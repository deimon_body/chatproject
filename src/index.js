import React from "react";
import ReactDOM from "react-dom";

import Chat from "./Chat.jsx";

ReactDOM.render(
  <React.StrictMode>
    <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
  </React.StrictMode>,
  document.getElementById("root")
);
