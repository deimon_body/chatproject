import { Component } from "react";
import "./styles/style.scss";
import Preloader from "./components/Loader/Loader.jsx";
import ChatHeader from "./components/ChatHeader/ChatHeader.jsx";

import MessageList from "./components/MessageList/MessageList.jsx";
import MessageInput from "./components/chatControl/MessageInput.jsx";
import getTimeHours from "./helpers/getTimeHours.js";

class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatData: [],
      isLoaded: false,
    };
  }

  getIndexFindElement(id) {
    return this.state.chatData.findIndex((obj) => {
      return obj.id === id;
    });
  }

  setData = () => {
    fetch(`${this.props.url}`)
      .then((res) => res.json())
      .then((data) => {
        data = data.map((obj) => {
          return {
            user: obj.user,
            text: obj.text,
            createdAt: getTimeHours(Date.parse(obj.createdAt)),
            avatar: obj.avatar,
            id: obj.id,
            isLiked: false,
          };
        });
        this.setState({
          chatData: data,
          isLoaded: true,
        });
      });
  };

  componentDidMount() {
    this.setData();
  }

  addNewMessage = (newMessageObject) => {
    let currentData = this.state.chatData;
    currentData.push(newMessageObject);
    this.setState({
      chatData: currentData,
    });
  };

  deleteMessage = (id) => {
    const currentData = this.state.chatData;
    currentData.splice(this.getIndexFindElement(id), 1);
    this.setState({
      chatData: currentData,
    });
  };

  editMessage = (newText, id) => {
    if (newText.length < 1) return;
    const currentData = this.state.chatData;
    currentData[this.getIndexFindElement(id)].text = newText;
    this.setState({
      chatData: currentData,
    });
  };

  likeClicker = (id) => {
    const currentData = this.state.chatData;
    currentData[this.getIndexFindElement(id)].isLiked =
      !currentData[this.getIndexFindElement(id)].isLiked;
    this.setState({
      chatData: currentData,
    });
  };

  render() {
    return this.state.isLoaded ? (
      <section className="chat">
        <ChatHeader
          chatInfo={this.state.chatData}
          time={this.state.chatData[this.state.chatData.length - 1].createdAt}
        />
        <MessageList
          chatInfo={this.state.chatData}
          deleteMessage={this.deleteMessage}
          editMessage={this.editMessage}
          likeClicker={this.likeClicker}
        />
        <MessageInput addMessage={this.addNewMessage} />
      </section>
    ) : (
      <Preloader />
    );
  }
}

export default Chat;
